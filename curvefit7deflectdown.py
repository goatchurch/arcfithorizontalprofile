import math
from curvefit7p2 import P2

"""
<TryaxWheelfile>
  <WheelHeader>
  ...
  </WheelHeader>
  <Wheelimage>
  </Wheelimage>
  
  <deflectnode>
    <x>853.2635</x>
    <dy>-1</dy>
    <smoothr>10</smoothr>
  </deflectnode>
  <deflectnode>
    <x>785.3476</x>
    <dy>0</dy>
    <smoothr>10</smoothr>
  </deflectnode>
"""

def ExtractDeflectNodes(xmltag):
    deflectnodes = [ dict((x, float(v))  for x, v in xv)  for xk, xv in xmltag[0][1]  if xk == "deflectnode" ]
    deflectnodes.sort(key=lambda X: X["x"])
    for i in range(1, len(deflectnodes)):
        c1 = P2(deflectnodes[i]["x"], deflectnodes[i]["dy"])
        c0 = P2(deflectnodes[i-1]["x"], deflectnodes[i-1]["dy"])
        smoothr = deflectnodes[i]["smoothr"]
        assert smoothr == deflectnodes[i-1]["smoothr"], "smoothr needs to be constant"
        sv = P2.ZNorm(P2.CPerp(c1 - c0))*smoothr
        assert sv.v < 0
        deflectnodes[i]["sp0"] = c0 + sv + P2(0, smoothr)
        deflectnodes[i]["sp1"] = c1 + sv + P2(0, smoothr)
    if deflectnodes:
        deflectnodes[0]["sp0"] = None
    return deflectnodes
    
def deflectatx(x, deflectnodes):
    if not deflectnodes:
        return 0
    if x <= deflectnodes[0]["x"]:
        return deflectnodes[0]["dy"]
    if x >= deflectnodes[-1]["x"]:
        return deflectnodes[-1]["dy"]
    ly = 0
    for dn in deflectnodes:
        dny2 = dn["smoothr"]**2 - (dn["x"] - x)**2
        if dny2 > 0:
            dny = math.sqrt(dny2)
            dnylo = dn["dy"] - (dn["smoothr"] - dny)
            dnyhi = dn["dy"] + (dn["smoothr"] - dny)
            ly = min(dnyhi, max(dnylo, ly))
        if dn["sp0"] is not None:
            lamx = (x - dn["sp0"].u)/(dn["sp1"].u - dn["sp0"].u)
            if 0 <= lamx <= 1:
                spdnylo = dn["sp0"].v*(1-lamx) + dn["sp1"].v*lamx
                ly = min(spdnylo, ly)
    return ly



