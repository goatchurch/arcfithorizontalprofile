import math, re, sys, os

lineartol = 0.05     # tolerance band which we fit lines
midlineartol = lineartol/4  # middle of the band should be within this distance of line
#lineartolext = 0.02  # tolerance we extend line segments into arc regions
arcfittol = 0.18      # tolerance we fit arc segments
tangentslopecorrectiontol = lineartol/2
maxarcrad=20000
slopemeasuresamplewidth = 5

try: 
    sendactivity  #  function used in twistcodewiki debugging framework 
    def dprint(*X):  sys.stdout.write(str(X)+"\n")
    sys.path.append("/home/goatchurch/geom3d/arcfithorizontalprofile")
    bTwistCodeWiki = True
except NameError:
    def sendactivity(*args, **kwargs):  pass
    def dprint(*X):  pass  # turn off printing
    bTwistCodeWiki = False

from curvefit7p2 import P2
from curvefit7xml import ParseSimpleXML, RewriteSimpleXML, ExtractPts, MakeXAscending, GenerateXMLsmooth, GenerateXMLsections
from curvefit7inflexions import DeriveSlopesCurves, FindInflexPoints, ShiftInflexToRoughPoints, RemoveInflexInConstRegions, ExtractInflexPointSlopes, AssignStraights, RemoveShortStraightsInCurves, MergeSameDirectionCurves, PlotInflexRegions, RetractingContinuousLinearSegments, RevalueInflexSlopes, RevalueSlopesBetweenInflex
from curvefit7deflectdown import ExtractDeflectNodes, deflectatx
from curvefit7biarc import BiCircle, AssignBicirclesLines, FixBicircs, FixBicirc, GetGSeq, PlotGseq, Gettolerance

if len(sys.argv) == 3:  
    filein, fileout = sys.argv[1], sys.argv[2]
elif not bTwistCodeWiki:
    print("Usage: %s <filein.xml> <fileout.xml>" % sys.argv[0])
    sys.exit(0)
    
pts = [ ]    
def Main(filein, fileout, yd):
    global pts
    if fileout:
        try:  os.remove(fileout)
        except OSError:  pass
    xmltitle, xmltag = ParseSimpleXML(filein)
    pts = ExtractPts(xmltag)
    deflectnodes = ExtractDeflectNodes(xmltag)
    #sendactivity(points=pts, materialnumber=1)
    pts = [P2(x, y+deflectatx(x, deflectnodes))  for x, y in pts]
    #sendactivity(points=pts, materialnumber=2)
    pts, bXreflected = MakeXAscending(pts)

    # (start subselecting here for rerunning in twistcodewiki)
    sendactivity(contours=[pts], materialnumber=1)
    sendactivity(points=pts, materialnumber=1)

    # inflexion point detection by using filtering
    slopes, csmooth, crough = DeriveSlopesCurves(pts, slopemeasuresamplewidth, yd=pts[0][1]-10, sendactivity=None)
    IIcinflex = FindInflexPoints(csmooth)
    IIcinflex = ShiftInflexToRoughPoints(IIcinflex, csmooth, crough)
    
    # Merge and assign curve types between the inflexion points
    cpinflex = ExtractInflexPointSlopes(IIcinflex, pts, slopes)
    flatcpis = RetractingContinuousLinearSegments(cpinflex, pts, lineartol, midlineartol)
    cpinflex = AssignStraights(cpinflex, flatcpis)
    cpinflex = RemoveShortStraightsInCurves(cpinflex, width=4)
    cpinflex = MergeSameDirectionCurves(cpinflex)
    RevalueInflexSlopes(pts, slopemeasuresamplewidth, cpinflex, slopes, tangentslopecorrectiontol, dprint)
    slopes = RevalueSlopesBetweenInflex(cpinflex, slopes, pts, slopemeasuresamplewidth, dprint)
    
    PlotInflexRegions(cpinflex, sendactivity)

    AssignBicirclesLines(cpinflex, pts, maxarcrad, dprint)
    FixBicircs(cpinflex, pts, slopes, arcfittol, maxarcrad, sendactivity, dprint)

    # to rerun an individual:
    # cpinflex[j].biarcs = [ cpinflex[j].Dbiarc0 ]
    # FixBicirc(cpinflex[j], pts, slopes, tol, maxarcrad, dprint)
    gseq = GetGSeq(cpinflex)
    PlotGseq(gseq, pts, yd, sendactivity)
    xmlcfp = ("curvefit7", [ ])
    xmlcfp[1].append(("tolerance", str(Gettolerance(pts, gseq, dprint))))
    xmlcfp[1].append(("smoothcurve", GenerateXMLsmooth(gseq, bXreflected)))
    xmlcfp[1].append(("dirsections", GenerateXMLsections(cpinflex, bXreflected)))
    xmltag[0][1].insert(0, xmlcfp)
    if fileout:
        RewriteSimpleXML(fileout, xmltitle, xmltag)

        
        
# call out if we're running from command line
if not bTwistCodeWiki:
    Main(filein, fileout, 0)
    sys.exit(0)

    
sendactivity("clearalltriangles")
sendactivity("clearallpoints")
sendactivity("clearallcontours")




# junk debug testing code    
fdir = "/home/goatchurch/geom3d/arcfithorizontalprofile/testdata/"
fnames = [ f  for f in os.listdir(fdir)  if re.search(".xml$", f) and not re.search("smoothed", f) ]
for i, f in enumerate(fnames):
    break
    filein = os.path.join(fdir, f)
    try:
        Main(filein, None, i*50+1)
    except Exception as e:
        print(e)
    if i > 30:  break
        
filein = os.path.join(fdir, "Merc 8.5x18 H24011302.cnc.xml")
#filein = os.path.join(fdir, "aftermarket 7.5x17 et40 TSE A356.2.cnc.xml")
filein = "/home/goatchurch/geom3d/arcfithorizontalprofile/smoothtemp.xml"
filein = "/home/goatchurch/geom3d/arcfithorizontalprofile/honda test v4.xml"
yd = 0
Main(filein, None, yd)



