import math
from curvefit7p2 import P2

# represents the inflexion segment up to this point from the previous inflexion segment
# list of which is generated in ExtractInflexPointSlopes
class CPIinflex:
    def __init__(self, i, slope, pt, direction):
        self.i = i
        self.slope = slope
        self.pt = pt
        self.direction = direction   # "curveup", "curvedown", "straight"
        self.biarcs = None


def slopecurve(pts, i, slopemeasuresamplewidth):
    # balance from the end extensions so we the width is symetrically narrowed down, 
    # which means we are perfectly tangent with the end pair of points
    ir = max(1, min(i, len(pts) - 1 - i, slopemeasuresamplewidth))  
    ps = pts[max(i-ir,0):min(i+ir+1,len(pts))]
    Nfac = 1.0/len(ps)
    su, sv = sum(p.u  for p in ps)*Nfac, sum(p.v  for p in ps)*Nfac
    su2, suv = sum(p.u**2  for p in ps)*Nfac, sum(p.u*p.v  for p in ps)*Nfac
    m = (suv - su*sv) / (su2 - su**2)
    c = (sv - m*su)
    miderr = pts[i].v - (m*pts[i].u + c)
    return m, miderr

# segmentation work
def convolve(seq, kern):
    res = [ ]
    for i in range(len(seq)):
        sv, s = kern[0]*seq[i], kern[0]
        for j in range(1, len(kern)):
            if i+j < len(seq):
                sv += kern[j]*seq[i+j]
                s += kern[j]
            if i-j >= 0:
                sv += kern[j]*seq[i-j]
                s += kern[j]
        res.append(sv/s)
    return res

def DeriveSlopesCurves(pts, slopemeasuresamplewidth, yd, sendactivity):
    kernWide = [math.exp(-x**2*0.04)  for x in range(11)]
    kernNarrow = [math.exp(-x**2*0.2)  for x in range(5)]

    scs = [ slopecurve(pts, i, slopemeasuresamplewidth)  for i in range(len(pts)) ]
    slopes = [ sc[0]  for sc in scs ]
    curvature = [ sc[1]  for sc in scs ]
    
    csmooth = convolve(curvature, kernWide)
    crough = convolve(curvature, kernNarrow)
    if sendactivity:
        sendactivity(contours=[[(pts[0][0]-5,yd),(pts[-1][0]+5,yd)]]) 
        sendactivity(contours=[[(pts[i][0], curvature[i]*200+yd)  for i in range(len(pts))]], materialnumber=3)
        sendactivity(contours=[[(pts[i][0], csmooth[i]*200+yd)  for i in range(len(pts))]], materialnumber=1)
        sendactivity(contours=[[(pts[i][0], crough[i]*200+yd)  for i in range(len(pts))]], materialnumber=2)
    return slopes, csmooth, crough


def FindInflexPoints(csmooth):
    cinflex = [ 0 ]
    for i in range(1, len(csmooth)):
        if (csmooth[i] < 0) != (csmooth[cinflex[-1]] < 0):
            cinflex.append(i)
    cinflex.append(len(csmooth))
    return cinflex


def ShiftInflexToRoughPoints(lcinflex, csmooth, crough):
    cinflex = lcinflex[:]
    for j in range(1, len(cinflex)-1):
        bdown = (csmooth[cinflex[j]] < 0)
        for nv in (cinflex[j] + ik*d  for ik in range(10)  for d in [-1,1]):
            if not cinflex[j-1] < nv < cinflex[j+1]:
                continue
            if ((crough[nv-1] < 0) != bdown) and ((crough[nv] < 0) == bdown):
                cinflex[j] = nv
                break
    return cinflex

def RemoveInflexInConstRegions(lcinflex, slopes, minslopediff):
    cinflex = lcinflex[:]
    def rg(L):
        return max(L)-min(L)
    while len(cinflex) > 2:
        slopediff, j = min((rg([slopes[i]  for i in range(cinflex[j-1], cinflex[j+1])]), j)   for j in range(1, len(cinflex)-1))
        if slopediff > minslopediff:
            break
        del cinflex[j]
    return cinflex, slopediff

        
def ExtractInflexPointSlopes(cinflex, pts, slopes):
    cpinflex = [ ]
    for j in range(len(cinflex)):
        ci = cinflex[j]
        if ci == len(pts):
            ci -= 1
        if j != 0:
            dr = "curveup" if (slopes[ci] > slopes[cinflex[j-1]]) else "curvedown"
        else:
            dr = "start"
        cpinflex.append(CPIinflex(cinflex[j], slopes[ci], pts[ci], dr))
    return cpinflex



def CPItolrg(cpi0, cpi1, pts):
    p0, p1 = cpi0.pt, cpi1.pt
    ds = [ ]
    for p in pts[cpi0.i+1:cpi1.i]:
        lam = (p.u - p0.u)/(p1.u - p0.u)
        ds.append((p0.v*(1-lam) + p1.v*lam - p.v))
    return min(ds), max(ds)

def RetractingContinuousLinearSegments(cpinflex, pts, lineartol, midlineartol):
    # find the straight line tolerance for each segment
    flatcpis = [ [ ] ]
    for i in range(1, len(cpinflex)):
        minds, maxds = CPItolrg(cpinflex[i-1], cpinflex[i], pts)
        if max(abs(minds), abs(maxds)) < lineartol:
            flatcpis[-1].append(i)
        elif flatcpis[-1]:
            flatcpis.append([])
    if not flatcpis[-1]:
        flatcpis.pop()

    # merge sequences of straight line segments to within tolerance
    rflatcpis = [ ]
    while flatcpis:
        flatcpi = flatcpis.pop()
        flatcpidropfront = [ ]
        flatcpidropback = [ ]
        while True:
            minds, maxds = CPItolrg(cpinflex[flatcpi[0]-1], cpinflex[flatcpi[-1]], pts)
            if max(abs(minds), abs(maxds)) <= lineartol and abs((minds + maxds)/2) <= midlineartol:
                rflatcpis.append(flatcpi)
                break
            if len(flatcpi) == 1:
                break
            backtol = max(map(abs, CPItolrg(cpinflex[flatcpi[1]-1], cpinflex[flatcpi[-1]], pts)))
            fronttol = max(map(abs, CPItolrg(cpinflex[flatcpi[0]-1], cpinflex[flatcpi[-2]], pts)))
            if backtol < fronttol:
                flatcpidropfront.append(flatcpi.pop(0))
            else:
                flatcpidropback.insert(0, flatcpi.pop(-1))
        if flatcpidropfront:
            flatcpis.append(flatcpidropfront)
        if flatcpidropback:
            flatcpis.append(flatcpidropback)
    rflatcpis.sort()
    return rflatcpis   # [ [ ints ] ]
    

def AssignStraights(cpinflex, flatcpis):
    cpinflex = cpinflex[:]
    while flatcpis:
        flatcpi = flatcpis.pop()
        p0, p1 = cpinflex[flatcpi[0]-1].pt, cpinflex[flatcpi[-1]].pt
        slope = (p1.v - p0.v)/(p1.u - p0.u)
        cpinflex[flatcpi[0]-1].slope = slope
        cpinflex[flatcpi[-1]].slope = slope
        cpinflex[flatcpi[-1]].direction = "straight"
        del cpinflex[flatcpi[0]:flatcpi[-1]]
    return cpinflex


"""
# this would need to revalue the slopes at the ends
def ExtendStraights(cpinflex)
    j = 3
    cpiprev = cpinflex[j - 1]
    cpi = cpinflex[j]
    assert cpiprev.slope == cpi.slope and cpi.direction == "straight"
    i = cpiprev.i
    cpiprev2 = cpinflex[j - 2]
    while i > cpiprev2.i + 1:
        ev = cpiprev.pt - pts[i-1]
        if abs(ev.u*cpi.slope - ev.v) < lineartolext:
            i -= 1
        else:
            break
    i = cpiprev.i
    cpi.pt
"""

def RemoveShortStraightsInCurves(lcpinflex, width):
    cpinflex = lcpinflex[:]
    j = 0
    while j < len(cpinflex) - 2:
        j += 1
        if cpinflex[j].direction != "straight":
            continue
        if cpinflex[j].pt.u - cpinflex[j-1].pt.u > width:
            continue
            
        if j == 1:
            if cpinflex[j+1].direction != "straight":
                del cpinflex[j]
                j = 0
        elif j == len(cpinflex) - 1:
            if cpinflex[j-1].direction != "straight":
                cpinflex[j].direction = cpinflex[j-1].direction
                del cpinflex[j-1]
                j = 0
        else:
            if cpinflex[j-1].direction == cpinflex[j+1].direction and cpinflex[j+1].direction != "straight":
                del cpinflex[j-1:j+1]
                j = 0
    return cpinflex

def MergeSameDirectionCurves(lcpinflex):
    cpinflex = lcpinflex[:]
    j = 0
    while j < len(cpinflex) - 2:
        if cpinflex[j].direction != "straight" and cpinflex[j].direction == cpinflex[j+1].direction:
            del cpinflex[j]
        else:
            j += 1
    return cpinflex

def PlotInflexRegions(cpinflex, sendactivity):
    conts = [ ]; 
    for j in range(1, len(cpinflex)):
        Dcv = P2(0, {"curveup":10, "curvedown":-10, "straight":0}[cpinflex[j].direction])
        conts.append([cpinflex[j-1].pt+Dcv, cpinflex[j].pt+Dcv])
    sendactivity(contours=[[cpx.pt+P2(0,100), cpx.pt-P2(0,100)]  for cpx in cpinflex])
    sendactivity(contours=[[cpx.pt-P2(1,cpx.slope)*10, cpx.pt+P2(1,cpx.slope)*10]  for cpx in cpinflex], materialnumber=2)
    sendactivity(contours=conts)


def RevalueInflexSlopes(pts, slopemeasuresamplewidth, cpinflex, slopes, tangentslopecorrectiontol, dprint):
    for j in range(1, len(cpinflex) - 1):
        cpi, cpin = cpinflex[j], cpinflex[j+1] 
        if cpi.direction == "curvedown" and cpin.direction == "curveup":
            fac = -1
        elif cpi.direction == "curveup" and cpin.direction == "curvedown":
            fac = 1
        else:
            continue
        flexslopeerrs = [ ]
        for i in range(cpi.i+1, cpin.i):
            v = pts[i] - cpi.pt
            vslope = v.v/v.u
            if vslope*fac > cpi.slope*fac:
                flexslopeerrs.append((v.u*(vslope - cpi.slope)*fac, vslope, i))
        for i in range(cpinflex[j-1].i+1, cpi.i):
            v = pts[i] - cpi.pt
            vslope = v.v/v.u
            if vslope*fac > cpi.slope*fac:
                flexslopeerrs.append((-v.u*(vslope - cpi.slope)*fac, vslope, i))
        flexslopeerrs.sort()
        dprint(cpi.i, fac, cpi.slope, cpi.pt)
        dprint(flexslopeerrs)
        while flexslopeerrs and flexslopeerrs[-1][0] > tangentslopecorrectiontol:
            flexslopeerr = flexslopeerrs.pop()
            if flexslopeerr[1]*fac > cpi.slope*fac:
                cpi.slope = flexslopeerr[1]
                dprint("revalue cpinflex slope", cpi.slope, flexslopeerr[1])
                slopes[cpi.i] = cpi.slope
                
def RevalueSlopesBetweenInflex(cpinflex, lslopes, pts, slopemeasuresamplewidth, dprint):
    slopes = lslopes[:]
    for j in range(1, len(cpinflex)):
        cpip, cpi = cpinflex[j-1], cpinflex[j] 
        if cpi.direction == "curvedown":
            fac = -1
        elif cpi.direction == "curveup":
            fac = 1
        else:
            continue

        for i in range(cpip.i+1, cpi.i):
            flexslopeerrs = [ ]
            slopelimlo, slopelimhi = None, None
            for i1 in range(max(cpip.i, i-slopemeasuresamplewidth), i):
                v = pts[i1] - pts[i]
                vslope = v.v/v.u
                if slopelimlo is None or vslope*fac > slopelimlo*fac:
                    slopelimlo = vslope
            for i1 in range(i+1, min(cpi.i, i+slopemeasuresamplewidth+1, len(pts)-1)+1):
                v = pts[i1] - pts[i]
                vslope = v.v/v.u
                if slopelimhi is None or vslope*fac < slopelimhi*fac:
                    slopelimhi = vslope
            if slopelimlo is not None and slopelimhi is not None and slopelimlo*fac > slopelimhi*fac:
                slopes[i] = (slopelimlo + slopelimhi)*0.5
                if abs(slopelimlo - slopelimhi) > 0.1:
                    dprint("concave!", pts[i], slopelimlo, slopelimhi)
            elif slopelimlo is not None and slopelimlo*fac > slopes[i]*fac:
                slopes[i] = slopelimlo
            elif slopelimhi is not None and slopes[i]*fac > slopelimhi*fac:
                slopes[i] = slopelimhi
    return slopes
