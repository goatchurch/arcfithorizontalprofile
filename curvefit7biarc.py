import math
from curvefit7p2 import P2

dprint = None
class BiCircle:
    def __init__(self, p0, n0, i0, p1, n1, i1, direction):
        self.p0, self.n0, self.p1, self.n1 = p0, n0, p1, n1
        self.i0, self.i1 = i0, i1
        self.direction = direction
        assert direction in ["curvedown", "curveup"]
        self.bsuccess = False
        self.MakeBiConsts()

    def MakeBiConsts(self):
        self.bcurvedown = (self.direction == "curvedown")
        p0, n0, p1, n1 = self.p0, self.n0, self.p1, self.n1
        s0 = P2.Dot(p1-p0, n1)/P2.Dot(P2.CPerp(n0), n1)
        s1 = P2.Dot(p0-p1, n0)/P2.Dot(P2.APerp(n1), n0)
        self.bs0longer = (s0 > s1)
        self.s0, self.s1 = s0, s1
        if self.s0 < 0 or self.s1 < 0:
            dprint("(p0, n0, p1, n1) = ", (p0, n0, p1, n1))
            dprint("s0s1bad", s0, s1)
            #sendactivity(points=[p0,p1], materialnumber=2)
            #sendactivity(contours=[(p0,p0+n0*3),(p1,p1+n1*3)], materialnumber=3)
            self.Q = None
            return
        self.Q = p0+P2.CPerp(n0)*s0
        self.Q = p1+P2.APerp(n1)*s1

        h1 = P2.Dot(P2.CPerp(n1), n0)*(s0+s1)*0.5/(1 - P2.Dot(n1, n0))
        h0 = P2.Dot(P2.APerp(n0), n1)*(s0+s1)*0.5/(1 - P2.Dot(n1, n0))
        self.C = self.Q + P2.CPerp(n1)*((s0+s1)*0.5) + n1*h1
        self.Cr = math.sqrt((s1-s0)**2/4+h0**2)

    def Findpm(self, lam, pts, dprint):
        p0, n0, p1, n1 = self.p0, self.n0, self.p1, self.n1
        C, Cr = self.C, self.Cr
        pp = None
        if pts:
            for i in range(self.i0, min(self.i1, len(pts)-1)):
                imr = (pts[i] - C).Len() - Cr
                if i != self.i0:
                    if ((imrprev < 0) and (imr >= 0)) if self.bs0longer else ((imrprev >= 0) and (imr < 0)):
                        llam = -imrprev/(imr - imrprev)
                        pp = pts[i-1]*(1 - llam) + pts[i]*llam
                        #dprint("Findpm", pp, llam, i, self.i0, self.i1)
                imrprev = imr
            if pp is None:
                dprint("mid-range not found")
                dprint("s0, s1, i0, i1, C, Cr = ", (self.s0, self.s1, self.i0, self.i1, C, Cr))
            
        if pp is None:
            assert 0<lam<1
            smin = self.s1 if self.bs0longer else self.s0
            assert smin == min(self.s0, self.s1)
            pp = self.Q + (P2.APerp(n0)*(1-lam) + P2.CPerp(n1)*lam)*smin
        self.pm = C + P2.ZNorm(pp - C)*Cr


    def CalcRadVals(self, maxarcrad):
        p0, n0, p1, n1 = self.p0, self.n0, self.p1, self.n1
        s0, s1 = self.s0, self.s1
        pm = self.pm
        rden0 = (2*P2.Dot(p0-pm, n0))
        rden1 = (2*P2.Dot(p1-pm, n1))

        if rden0 != 0:
            r0 = -(p0-pm).Lensq()/rden0
            br0large = (abs(r0) > maxarcrad)
        else:
            br0large = True
            
        if rden1 != 0:
            r1 = -(p1-pm).Lensq()/rden1
            br1large = (abs(r1) > maxarcrad)
        else:
            br1large = True

        if  br0large and br1large:
            return False
        elif br0large:
            r0 = 0  # signifies a flat line
            pm = self.Q + P2.CPerp(n0)*s1
            rden1 = (2*P2.Dot(p1-pm, n1))
            if rden1 == 0:
                return False
            r1 = -(p1-pm).Lensq()/rden1
            bres = ((r1 < 0) == self.bcurvedown)
        elif br1large:
            r1 = 0  # signifies a flat line
            pm = self.Q + P2.APerp(n1)*s0
            rden0 = (2*P2.Dot(p0-pm, n0))
            if rden0 == 0:
                return False
            r0 = -(p0-pm).Lensq()/rden0
            bres = ((r0 < 0) == self.bcurvedown)
        else:
            if self.bcurvedown:
                bres = (r0 < 0 and r1 < 0)
            else:
                bres = (r0 > 0 and r1 > 0)
            
        self.c0, self.c1 = p0+n0*r0, p1+n1*r1
        self.r0, self.r1 = r0, r1
        if r0 == 0:
            self.c0 = None   # force an error
        if r1 == 0:
            self.c1 = None
        return bres
            
            
    def MakeBiNew(self, lam, maxarcrad, pts, dprint):
        if self.Q is None:
            self.bsuccess = False
            return False  # cannot do anything
        self.Findpm(lam, pts, dprint)
        if self.CalcRadVals(maxarcrad):
            self.bsuccess = True
            return True

        if pts is not None:
            #dprint("try again")
            self.Findpm(lam, None, dprint)
            if self.CalcRadVals(maxarcrad):
                self.bsuccess = True
                return True
                
        self.bsuccess = False
        return False  # cannot do anything

    def GetTol(self, pts):
        self.tol = 0.0
        for i in range(self.i0+1, self.i1):
            p = pts[i]
            if self.bsuccess:
                if p.u < self.pm.u:
                    if self.r0 != 0:
                        tol = abs((p - self.c0).Len() - abs(self.r0))
                    else:
                        tol = lintoll(self.p0, p, self.pm)
                else:
                    if self.r1 != 0:
                        tol = abs((p - self.c1).Len() - abs(self.r1))
                    else:
                        tol = lintoll(self.pm, p, self.p1)
            else:
                tol = lintoll(self.p0, p, self.p1)
            if tol > self.tol:
                self.tol = tol
        return self.tol
        
    def PlotBi(self, sendactivity):
        sendactivity(points=(self.c0, self.c1))
        sendactivity(contours=[(self.c0, self.c1)])
        sendactivity(points=[self.pm], materialnumber=1)
    

def FixBicirc(cpi, pts, slopes, tol, maxarcrad, ldprint):
    global dprint
    dprint = ldprint
    k = -1
    while k < len(cpi.biarcs) - 1:
        k += 1
        bic = cpi.biarcs[k]
        btol = bic.GetTol(pts)
        if bic.bsuccess and btol <= tol:
            continue
        if not bic.bsuccess:
            #dprint("rejecting not success Bicirc")
            if btol <= tol:
                continue
            # fit straight lines down to the tolerance anyway
        im = (bic.i0 + bic.i1)//2
        if not bic.i0 < im < bic.i1:
            continue
        pm = pts[im]
        mm = slopes[im]
        nm = P2.ZNorm(P2(-mm,1))
        bic0 = BiCircle(bic.p0, bic.n0, bic.i0, pm, nm, im, bic.direction)
        bic1 = BiCircle(pm, nm, im, bic.p1, bic.n1, bic.i1, bic.direction)
        bic0.MakeBiNew(0.5, maxarcrad, pts, dprint)
        bic1.MakeBiNew(0.5, maxarcrad, pts, dprint)
        cpi.biarcs[k] = bic1
        cpi.biarcs.insert(k, bic0)
        k -= 1
        #break

def FixBicircs(cpinflex, pts, slopes, tol, maxarcrad, sendactivity, dprint):
    for cpi in cpinflex[1:]:
        if cpi.direction != "straight":
            FixBicirc(cpi, pts, slopes, tol, maxarcrad, dprint)
            for bic in cpi.biarcs:
                if bic.bsuccess:
                    bic.PlotBi(sendactivity)


def lintoll(p0, p, p1):
    lam = (p.u - p0.u)/(p1.u - p0.u)
    return abs(p0.v*(1-lam) + p1.v*lam - p.v)

        
def AssignBicirclesLines(cpinflex, pts, maxarcrad, ldprint):
    global dprint
    dprint = ldprint
    for j in range(1, len(cpinflex)):
        p0, p1 = cpinflex[j-1].pt, cpinflex[j].pt
        i0, i1 = cpinflex[j-1].i, cpinflex[j].i
        if cpinflex[j].direction == "straight":
            cpinflex[j].tol = max(lintoll(p0, pts[i], p1)  for i in range(i0+1,i1))
            continue
        m0, m1 = cpinflex[j-1].slope, cpinflex[j].slope
        n0, n1 = P2.ZNorm(P2(-m0,1)), P2.ZNorm(P2(-m1,1))
        bic = BiCircle(p0, n0, i0, p1, n1, i1, cpinflex[j].direction)
        bic.MakeBiNew(0.5, maxarcrad, pts, dprint)
        cpinflex[j].Dbiarc0 = bic
        cpinflex[j].biarcs = [ bic ]



def GetGSeq(cpinflex):
    gseq = [ ]    
    for j in range(1, len(cpinflex)):
        p0, p1 = cpinflex[j-1].pt, cpinflex[j].pt
        if cpinflex[j].direction == "straight":
            assert not gseq or gseq[-1][-1] == p0
            gseq.append((p0, p1))
            continue
        for bic in cpinflex[j].biarcs:
            if bic.bsuccess:
                assert not gseq or gseq[-1][-1] == bic.p0
                gseq.append((bic.p0, bic.c0, bic.pm))
                assert not gseq or gseq[-1][-1] == bic.pm
                gseq.append((bic.pm, bic.c1, bic.p1))
            else:
                assert not gseq or gseq[-1][-1] == bic.p0
                gseq.append((bic.p0, bic.p1))   # unsuccessful become line segments
    return gseq

def PlotGseq(gseq, pts, yd, sendactivity):
    cconts, epts = [ ], [ ]
    lconts, lepts = [ ], [ ]
    for linarc in gseq:
        if len(linarc) == 3:
            p0, c, p2 = linarc
            rad = (p0 - c).Len()
            rad2 = (p2 - c).Len()
            #assert abs(rad - rad2) < 1e-4, (rad, rad2)
            cont = [ ]
            for k in range(51):
                lam = k/50.0
                m = p0*(1-lam) + p2*lam
                #cont.append(m)
                #assert (m-c).Len() != 0, (p0, c, p2)
                cont.append(c + P2.ZNorm(m-c)*rad)
            #cconts.append([p1,p1+P2(0,1)])
            cconts.append(cont)
            epts.extend([p0, p2])
        else:
            lconts.append(linarc)
            lepts.extend(linarc)
    if yd != 0:
        yv = P2(0,yd)-epts[0]
    else:
        yv = P2(0,0)
    if yd:
        sendactivity(contours=[[p+yv for p in pts] for lc in cconts], materialnumber=1)
        sendactivity(points=[p+yv for p in pts], materialnumber=1)
    sendactivity(contours=[[p+yv for p in lc] for lc in cconts], materialnumber=3)
    sendactivity(points=[p+yv for p in epts], materialnumber=3)
    sendactivity(contours=[[p+yv for p in lc] for lc in lconts], materialnumber=0)
    sendactivity(points=[p+yv for p in lepts], materialnumber=2)
    
    


def Gettolerance(pts, linarcseq, dprint):
    rad0s = [ (linarc[0]-linarc[1]).Len()  for linarc in linarcseq  if len(linarc)==3 ]
    rad2s = [ (linarc[2]-linarc[1]).Len()  for linarc in linarcseq  if len(linarc)==3 ]
    raderr = min(abs(r0-r1)  for r0,r1 in zip(rad0s, rad2s))
    perr = [ ]
    nlines = sum(1  for linarc in linarcseq  if len(linarc)==2)
    narcs = sum(1  for linarc in linarcseq  if len(linarc)==3)
    dprint("%d lines converted to %d lines and %d arcs" % (len(pts)-1, nlines, narcs))
    for p in pts:
        for linarc in linarcseq:
            p0, p2 = linarc[0], linarc[-1]
            if (p0.u<=p.u<=p2.u  if p0.u<=p2.u  else  p2.u<=p.u<=p0.u):
                break
        lam = (p.u - p0.u)/(p2.u - p0.u)
        assert -1e-5<=lam<=1+1e-5, (p, lam)
        if len(linarc) == 2:
            perr.append(abs(p0.v + lam*(p2.v - p0.v) - p.v))
        else:
            r = (p0 - linarc[1]).Len()
            perr.append(abs((p - linarc[1]).Len() - r))
    return max(perr)
        
