import re, math
from curvefit7p2 import P2

def GenerateXMLsections(cpinflex, bXreflected):    
    xfac = -1 if bXreflected else 1
    res = [ ]
    for i in range(1, len(cpinflex)):
        cpiprev, cpi = cpinflex[i-1], cpinflex[i]
        seqxml = [("x0", cpiprev.pt.u*xfac), ("z0", cpiprev.pt.v)]
        seqxml.append(("direction", cpi.direction))
        seqxml.extend([("x1", cpi.pt.u*xfac), ("z1", cpi.pt.v)])
        res.append(("dirsection", seqxml))
    return res
    
def GenerateXMLsmooth(linarcseq, bXreflected):
    xfac = -1 if bXreflected else 1
    res = [ ]
    for linarc in linarcseq:
        if len(linarc) == 2:
            #ent = EWdraw1.Line(New Object() {XOLDVAL, YOLDVAL, 0}, New Object() {xvalueto, Yvalueto, 0})
            seqxml = [ ("gcode", "G1") ]
            seqxml.extend([ ("x0", linarc[0][0]*xfac), ("z0", linarc[0][1]) ])
            seqxml.extend([ ("xc", 0), ("zc", 0) ])
            seqxml.extend([ ("x1", linarc[1][0]*xfac), ("z1", linarc[1][1]) ])
            seqxml.extend([ ("r", 0), ("msang", 0), ("meang", 0) ])
        else:
            #ent = EWdraw1.Arc(New Object() {mXcentr, mYcentr, 0}, mrad, meang, msang, New Object() {0, 0, 1})
            assert len(linarc) == 3
            vc = linarc[1] - linarc[0]
            bcircdir = (P2.Dot(P2.CPerp(linarc[2]-linarc[0]), vc) > 0)
            dc = 2 if (bcircdir != bXreflected) else 3
            seqxml = [ ("gcode", "G%d"%dc) ]
            seqxml.extend([ ("x0", linarc[0][0]*xfac), ("z0", linarc[0][1]) ])
            seqxml.extend([ ("xc", linarc[1][0]*xfac), ("zc", linarc[1][1]) ])
            seqxml.extend([ ("x1", linarc[2][0]*xfac), ("z1", linarc[2][1]) ])
            msvec, mevec = linarc[0] - linarc[1], linarc[2] - linarc[1]
            assert abs(msvec.Len() - mevec.Len()) < 0.001
            seqxml.extend([ ("r", mevec.Len()), ("msang", math.degrees(math.atan2(msvec.v, msvec.u*xfac))), ("meang", math.degrees(math.atan2(mevec.v, mevec.u*xfac))) ])
        res.append(("SmoothEntity", seqxml))
    return res

    
def ParseSimpleXML(filein):  # without attributes, one line per record
    flines = open(filein).readlines()
    xmltagstack = [ [ ] ]
    for fline in flines[1:]:
        m = re.match("\s*<(/)?(\w+)>(?:([^<]+)</(\w+)>)?\s*$", fline)
        assert m, fline
        if m.group(1):
            xmltagstack.pop()
            assert xmltagstack[-1][-1][0] == m.group(2)
        elif m.group(4):
            assert m.group(4) == m.group(2)
            xmltagstack[-1].append((m.group(2), m.group(3)))
        else:
            xmltagstack[-1].append((m.group(2), []))
            xmltagstack.append(xmltagstack[-1][-1][1]) # the []
    return flines[0], xmltagstack[0]

def ExtractPts(xmltag):
    assert len(xmltag) == 1 and xmltag[0][0] == "TryaxWheelfile"
    probedatas = [ xv  for xk, xv in xmltag[0][1]  if xk == "Probedata" ]
    pts = [ ]
    for probedata in probedatas:
        assert probedata[0][0] == "Probexvalue" and probedata[1][0] == "Probezvalue"
        pts.append(P2(float(probedata[0][1]), float(probedata[1][1])))
    return pts

def RewriteSimpleXML(fileout, xmltitle, xmltag):
    xmltagstack = [ [xmltag, 0] ]
    fout = open(fileout, "w")
    fout.write(xmltitle)
    while xmltagstack:
        blks = "  "*(len(xmltagstack)-1)
        xi = xmltagstack[-1][1]
        if xi < len(xmltagstack[-1][0]):
            xv = xmltagstack[-1][0][xi]
            if type(xv[1]) == list:
                fout.write("%s<%s>\n" % (blks, xv[0]))
                xmltagstack.append([xv[1], 0])
            else:
                fout.write("%s<%s>%s</%s>\n" % (blks, xv[0], xv[1], xv[0]))
                xmltagstack[-1][1] += 1
        else:
            xmltagstack.pop()
            if not xmltagstack:  break
            xi = xmltagstack[-1][1]
            fout.write("%s</%s>\n" % (blks[2:], xmltagstack[-1][0][xi][0]))
            xmltagstack[-1][1] += 1
    fout.close()

def MakeXAscending(pts):
    if pts[0].u < pts[-1].u:
        return pts, False
    return [P2(-p.u, p.v)  for p in pts], True
    
