# Description #

This code is for fitting a series of tangential arcs and straight lines to a set of 
sample points given in G-code of approx 0.2mm apart in X on a broadly horizontal profile.  

The *sendactivity()* function is part of the [twistcodewiki](https://bitbucket.org/goatchurch/twistcodewiki)
framework that allows for easier interactive debugging of this geometric algorithm. 

The main operation of the algorithm is to first discover the points of inflexion in order to 
segment the curve into straight, curveup and curvedown sections using a the slope and a 
heavily smoothed filter of a curature metric to get rid of any local dints.

Then, once the curve is segmented, the straight lines have their global tangential values 
imposed at their ends, and tangential arcs are fitted in the one-way curved sections up to tolerance.
It's only possible to do this with pairs of arcs between the nodes.  

The code for this is not complete as the splits points of the arcs are in the middle and not 
yet optimized for smoothness or curve fitting.  At the moment this can produce a 
series of consecutive arcs with wildly alternating radii.

# License #

This code is released under [BSD license](http://choosealicense.com/licenses/bsd-2-clause/), 
which permits you to do anything you like with it except blame me for the consequences.



