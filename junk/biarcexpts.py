from basicgeo import P2
import math

sendactivity("clearalltriangles")
sendactivity("clearallpoints")
sendactivity("clearallcontours")

p0, n0 = P2(0,0), P2(0, 1)
p1, n1 = P2(3,6.4), P2.ZNorm(P2(-3.7,1))
lam = 0.5
maxarcrad = 800

sendactivity(contours=[(p0,p0+n0*9), (p1,p1+n1*9)], materialnumber=2)
sendactivity(contours=[(p0+P2.APerp(n0)*5,p0,p0-P2.APerp(n0)*5), (p1+P2.APerp(n1)*5,p1,p1-P2.APerp(n1)*5)], materialnumber=0)

p = P2(P2.Dot(P2.CPerp(n0), p1 - p0), P2.Dot(n0, p1 - p0))
n = P2(P2.Dot(P2.CPerp(n0), n1), P2.Dot(n0, n1))
# vc = (p + n*s) - P2(0,r) ; vector between circle centres
# solve vc^2 = (r-s)^2
# p^2 + s^2 + r^2 + 2p.n*s - 2p_v*r - 2n_v*r*s = r^2 - 2r*s + s^2
# p^2 + 2p.n*s - 2p_v*r - 2n_v*r*s = -2r*s
c, cs, cr, crs = p.Lensq(), 2*P2.Dot(p, n), -2*p.v, -2*n.v + 2
# c + cs*s + cr*r + crs*r*s = 0
# s = -(c + cr*r)/(cs + crs*r)
# c + cr*r + s*cs + crs*r*s

#vc = (p + n*s) - P2(0,r)
#c0, c1 = p0 + n0*r, p1 + n1*s
#c1-c0
#vc.Lensq() - (r-s)**2
#P2.Dot(nhperp, vc)

lam = 0.5
lam = 0.1
rs = [ ]
cdiag = [ ]
def RRR(lam):
    # find at the halfway point where the 2 circles will be tangential
    nh = n*lam + P2(0, 1)*(1-lam)
    # solve vc.APerp(nh) = 0
    nhperp = P2.APerp(nh)
    # 0 = (p+n*s - P2(0,r)) . nhperp
    pdnhperp = P2.Dot(p, nhperp)
    ndnhperp = P2.Dot(n, nhperp)
    # 0 = pdnhperp + ndnhperp*s - nhperp.v*r
    # s = -(c + cr*r)/(cs + crs*r)
    # 0 = pdnhperp + ndnhperp*s - nhperp_v*r
    #   = pdnhperp - ndnhperp*(c + cr*r)/(cs + crs*r) - nhperp_v*r
    #   = pdnhperp*(cs + crs*r) - ndnhperp*(c + cr*r) - nhperp_v*r*(cs + crs*r)
    qa = -nhperp.v*crs
    qb2 = (pdnhperp*crs - ndnhperp*cr - nhperp.v*cs)/2
    qc = pdnhperp*cs - ndnhperp*c

    #pn*(cs + crs*r) - r*nperp.u*(cs + crs*r) - n.v*(c + cr*r)
    #s = -(c + cr*r)/(cs + crs*r)
    #P2.Dot(p,nperp) - r*nperp.u + p.v + n.v*s

    qdq = qb2**2 - qa*qc
    if qdq < 0 or qa == 0:
        assert False
    qs = math.sqrt(qdq) / qa
    qm = -qb2 / qa
    q0 = qm + qs
    q1 = qm - qs
    qa*q1**2 + 2*qb2*q1 + qc
    
    qss = [ ]
    den0, den1 = (cs + crs*q0), (cs + crs*q1)
    if den0 != 0:
        qss.append((q0, -(c + cr*q0)/den0))
    if den1 != 0:
        qss.append((q1, -(c + cr*q1)/den1))
    qss = [(r,s)  for r,s in qss  if r>0 and s>0 and max(r,s) < maxarcrad]
    qss.sort(key=lambda X:abs(X[0]-X[1]))
    bsuccess = False
    #r, s = qss[0]  vc.Len()
    for r, s in qss:
        c0, c1 = p0 + n0*r, p1 + n1*s
        assert abs((c0 - c1).Len() - abs(r - s)) < 1e-3 
        vc = P2.ZNorm(c1 - c0)
        m = c0 + vc*r*(+1 if P2.Dot(vc, n0)<0 else -1)
        assert abs((p0-c0).Len() - r) < 1e-4
        assert abs((p1-c1).Len() - s) < 1e-4
        rm, sm = (m-c0).Len(), (m-c1).Len()
        if abs(r - rm) > 1e-4 or abs(s - sm) > 1e-4:
            continue
        assert abs(r - rm) < 1e-4, (r, rm)
        assert abs(s - sm) < 1e-4, (s, sm)
        sendactivity(points=(c0, c1))
        sendactivity(contours=[(c0, c1)])
        sendactivity(points=[m], materialnumber=1)
        bsuccess = True
        print(P2.Dot(nhperp, c0-c1))
        rs.append((r, s))
        cdiag.append(m)
    return m

#for i in range(1,50):  RRR(i/50)
i, N = 1, 50
while i < N:
    RRR(i/N)
    i += 1

#self.c0, self.pm, self.c1 = c0, m, c1
#self.r0, self.r1 = rm, sm
if False and bsuccess:
    arc0 = [c0+P2.ZNorm(mm-c0)*r  for mm in (p0*(1-lam)+m*lam  for lam in (k/50.0  for k in range(51)))]
    arc1 = [c1+P2.ZNorm(mm-c1)*s  for mm in (m*(1-lam)+p1*lam  for lam in (k/50.0  for k in range(51)))]
    sendactivity(contours=[arc0, arc1], materialnumber=3)


import scipy.optimize    
def fun(x):
    cx, cy, r = x
    return sum((math.sqrt((c[0] - cx)**2 + (c[1] - cy)**2) - r)**2  for c in cdiag)
sendactivity(contours=[cdiag], materialnumber=1)

x0 = (0,4,4)
bnds = None#((-500,500), (0.001,0.05), (-500,500), (0.001,0.05), (-500,500), (0.001,0.05))
g = scipy.optimize.minimize(fun, x0, method=None, bounds=bnds, options={"maxiter":500}) 
cx, cy, r = g.x
sendactivity(contours=[[(cx+math.sin(math.radians(d))*r, cy+math.cos(math.radians(d))*r)  for d in range(361)]])
print(g.fun)

