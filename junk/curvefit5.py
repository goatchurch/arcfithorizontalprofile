"""Copyright (c) 2015, Julian Todd
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."""

import math, re, sys, os
from collections import namedtuple

# hard-coded file names to avoid having to do this from a secondary script
filein, fileout = "c:\\pythong1g23\\ay.cnc", "c:\\pythong1g23\\tyop.cnc"

if len(sys.argv) == 3:  # or set from command line arguments
    filein, fileout = sys.argv[1], sys.argv[2]
#print("Processing", filein, fileout)

# must first delete the fileout so failure isn't mistaken for previous successful output
try:  os.remove(fileout)
except OSError:  pass

try: 
    sendactivity  #  function is used from within the https://bitbucket.org/goatchurch/twistcodewiki visual debugging framework 
    def dprint(*X):  sys.stdout.write(str(X)+"\n")
    iA = 0   # set test geometry here
    filein = "/home/goatchurch/geom3d/arcfithorizontalprofile/testdata/a%d.cnc"%iA
    fileout = "/home/goatchurch/geom3d/arcfithorizontalprofile/testdata/b%d.cnc"%iA
        
except NameError:
    def sendactivity(*args, **kwargs):  pass
    def dprint(*X):  pass  # turn off printing
    

def Main():
    gcodeheader, pts, gcodefooter = ParseFile(filein)
    
    pts, bXreflected = MakeXAscending(pts)
    #pts = [P2(p[0] - pts[0][0],p[1]-pts[0][1])  for p in pts]
    sendactivity(contours=[pts], materialnumber=1)
    sendactivity(points=pts, materialnumber=1)

    slopes, csmooth, crough = DeriveSlopesCurves(pts, slopemeasuresamplewidth=5)
    cinflex = FindInflexPoints(csmooth)
    cinflex = ShiftInflexToRoughPoints(cinflex, csmooth, crough)
    cinflex, slopediff = RemoveInflexInConstRegions(cinflex, slopes, minslopediff=0.02)
    dprint("max double range slopediff", slopediff)
    cpinflex = ExtractInflexPointSlopes(cinflex, pts, slopes)
    AssignLinesPInflex(cpinflex, pts, lineartol=0.05)
    cpinflex = RemoveShortStraightsInCurves(cpinflex, width=4)
    cpinflex = MergeSameDirectionCurves(cpinflex)
    sendactivity(contours=[[cpx.pt+P2(0,100), cpx.pt-P2(0,100)]  for cpx in cpinflex])
    AssignBicirclesLines(cpinflex, pts, maxarcrad=20000)
    for cpi in cpinflex[1:]:  
        if cpi.direction != "straight":
            FixBicircs(cpi, pts, slopes, tol=0.2, maxarcrad=20000)
    gseq = GetGSeq(cpinflex)
    PlotGseq(gseq)
    gcodemiddle = GenerateGcodeMiddle(gseq, bXreflected)
    gstats = Getstats(pts, gseq)
    OutputGcode(fileout, gcodeheader, gcodemiddle, gcodefooter, gstats)
    dprint(gstats)
    

def OutputGcode(fileout, gcodeheader, gcodemiddle, gcodefooter, gstats):
    fout = open(fileout, "w")
    fout.write(gcodeheader)
    fout.write(gcodemiddle)
    if gcodemiddle[-1] != '\n':
        fout.write("\n")
    fout.write(gcodefooter)
    if not gcodefooter or gcodefooter[-1] != '\n':
        fout.write("\n")
    fout.write(gstats)
    fout.close()
    

def FixBicircs(cpi, pts, slopes, tol, maxarcrad):
    k = -1
    while k < len(cpi.bics) - 1:
        k += 1
        bic = cpi.bics[k]
        btol = bic.GetTol(pts)
        if bic.bsuccess and btol <= tol:
            continue
        im = (bic.i0 + bic.i1)//2
        if not bic.i0 < im < bic.i1:
            continue
        pm = pts[im]
        mm = slopes[im]
        nm = P2.ZNorm(P2(-mm,1))
        if bic.direction == "curvedown":
            nm = -nm
        bic0 = BiCircle(bic.p0, bic.n0, bic.i0, pm, nm, im, bic.direction)
        bic1 = BiCircle(pm, nm, im, bic.p1, bic.n1, bic.i1, bic.direction)
        bic0.MakeBi(0.5, maxarcrad)
        bic1.MakeBi(0.5, maxarcrad)
        cpi.bics[k] = bic1
        cpi.bics.insert(k, bic0)
        k -= 1

def lintoll(p0, p, p1):
    lam = (p.u - p0.u)/(p1.u - p0.u)
    return abs(p0.v*(1-lam) + p1.v*lam - p.v)

        
def AssignBicirclesLines(cpinflex, pts, maxarcrad):
    for j in range(1, len(cpinflex)):
        p0, p1 = cpinflex[j-1].pt, cpinflex[j].pt
        i0, i1 = cpinflex[j-1].i, cpinflex[j].i
        if cpinflex[j].direction == "straight":
            cpinflex[j].tol = max(lintoll(p0, pts[i], p1)  for i in range(i0+1,i1))
            continue
        m0, m1 = cpinflex[j-1].slope, cpinflex[j].slope
        n0, n1 = P2.ZNorm(P2(-m0,1)), P2.ZNorm(P2(-m1,1))
        if cpinflex[j].direction == "curvedown":
            n0, n1 = -n0, -n1
        bic = BiCircle(p0, n0, i0, p1, n1, i1, cpinflex[j].direction)
        bic.MakeBi(0.5, maxarcrad)
        cpinflex[j].bics = [ bic ]

def GetGSeq(cpinflex):
    gseq = [ ]    
    for j in range(1, len(cpinflex)):
        p0, p1 = cpinflex[j-1].pt, cpinflex[j].pt
        if cpinflex[j].direction == "straight":
            assert not gseq or gseq[-1][-1] == p0
            gseq.append((p0, p1))
            continue
        for bic in cpinflex[j].bics:
            if bic.bsuccess:
                assert not gseq or gseq[-1][-1] == bic.p0
                gseq.append((bic.p0, bic.c0, bic.pm))
                assert not gseq or gseq[-1][-1] == bic.pm
                gseq.append((bic.pm, bic.c1, bic.p1))
            else:
                assert not gseq or gseq[-1][-1] == bic.p0
                gseq.append((bic.p0, bic.p1))
    return gseq

def PlotGseq(gseq):
    cconts, epts = [ ], [ ]
    for linarc in gseq:
        if len(linarc) == 3:
            p0, c, p2 = linarc
            rad = (p0 - c).Len()
            rad2 = (p2 - c).Len()
            #assert abs(rad - rad2) < 1e-4, (rad, rad2)
            cont = [ ]
            for k in range(51):
                lam = k/50.0
                m = p0*(1-lam) + p2*lam
                #cont.append(m)
                cont.append(c + (m-c)*(rad/(m-c).Len()))
            #cconts.append([p1,p1+P2(0,1)])
            cconts.append(cont)
            epts.extend([p0, p2])
        else:
            cconts.append(linarc)
    sendactivity(contours=cconts, materialnumber=3)
    sendactivity(points=epts, materialnumber=3)
    
    
class BiCircle:
    def __init__(self, p0, n0, i0, p1, n1, i1, direction):
        self.p0, self.n0, self.p1, self.n1 = p0, n0, p1, n1
        self.i0, self.i1 = i0, i1
        self.direction = direction
        assert direction in ["curvedown", "curveup"]
        self.bsuccess = False

    def GetTol(self, pts):
        self.tol = 0.0
        for i in range(self.i0+1, self.i1):
            p = pts[i]
            if self.bsuccess:
                if p.u < self.pm.u:
                    tol = abs((p - self.c0).Len() - self.r0)
                else:
                    tol = abs((p - self.c1).Len() - self.r1)
            else:
                tol = lintoll(self.p0, p, self.p1)
            if tol > self.tol:
                self.tol = tol
        return self.tol
    
    def MakeBi(self, lam, maxarcrad):
        self.bsuccess = False
        p0, n0, p1, n1 = self.p0, self.n0, self.p1, self.n1
        p = P2(P2.Dot(n0, p1 - p0), P2.Dot(P2.APerp(n0), p1 - p0))
        n = P2(P2.Dot(n0, n1), P2.Dot(P2.APerp(n0), n1))
        # solve (p + n*s - (r,0))^2 = (r-s)^2
        # p^2 + s^2 + r^2 + 2p.n*s - 2p_u*r - 2n_u*r*s = r^2 - 2r*s + s^2
        # p^2 + 2p.n*s - 2p_v*r - 2n_v*r*s = -2r*s
        c, cs, cr, crs = p.Lensq(), 2*P2.Dot(p, n), -2*p.u, -2*n.u + 2
        # c + cs*s + cr*r + crs*r*s = 0
        # s = -(c + cr*r)/(cs + crs*r)

        # find at the halfway point where the 2 circles will be tangential
        nh = n*lam + P2(1, 0)*(1-lam)
        # solve vc = p+n*s - P2(r,0);  vc.APerp(nh) = 0
        nperp = P2.APerp(n)  
        # 0 = (p+n*s - P2(r,0)) . (nperp + (0,1))   !!! forces the halfway point of lam=1 anyway
        #   = p.nperp - r*nperp_u + p_v + n_v*s
        #   = p.nperp + p_v - r*nperp_u + n_v*s
        pn = P2.Dot(p, nperp) + p.v
        #   = pn*(cs + crs*r) - r*nhperp_u*(cs + crs*r) - n_v*(c + cr*r)
        qa = -nperp.u*crs
        qb2 = (pn*crs - nperp.u*cs - n.v*cr)/2
        qc = pn*cs - n.v*c

        #pn*(cs + crs*r) - r*nperp.u*(cs + crs*r) - n.v*(c + cr*r)
        #s = -(c + cr*r)/(cs + crs*r)
        #P2.Dot(p,nperp) - r*nperp.u + p.v + n.v*s

        qdq = qb2**2 - qa*qc
        if qdq < 0 or qa == 0:
            return False
        qs = math.sqrt(qdq) / qa
        qm = -qb2 / qa
        q0 = qm + qs
        q1 = qm - qs
        qa*q1**2 + 2*qb2*q1 + qc

        qss = [ ]
        den0, den1 = (cs + crs*q0), (cs + crs*q1)
        if den0 != 0:
            qss.append((q0, -(c + cr*q0)/den0))
        if den1 != 0:
            qss.append((q1, -(c + cr*q1)/den1))
        qss = [(r,s)  for r,s in qss  if r>0 and s>0 and max(r,s) < maxarcrad]
        qss.sort(key=lambda X:abs(X[0]-X[1]))
        for r, s in qss:
            c0, c1 = p0 + n0*r, p1 + n1*s
            assert abs((c0 - c1).Len() - abs(r - s)) < 1e-3 
            vc = P2.ZNorm(c1 - c0)
            m = c0 + vc*r*(+1 if P2.Dot(vc, n0)<0 else -1)
            assert abs((p0-c0).Len() - r) < 1e-4
            assert abs((p1-c1).Len() - s) < 1e-4
            rm, sm = (m-c0).Len(), (m-c1).Len()
            if abs(r - rm) > 1e-4 or abs(s - sm) > 1e-4:
                continue
            assert abs(r - rm) < 1e-4, (r, rm)
            assert abs(s - sm) < 1e-4, (s, sm)
            sendactivity(points=(c0, c1))
            sendactivity(contours=[(c0, c1)])
            sendactivity(points=[m], materialnumber=1)
            self.c0, self.pm, self.c1 = c0, m, c1
            self.r0, self.r1 = rm, sm
            self.bsuccess = True
            return True
        return False
    
    
class P2(namedtuple('P2', ['u', 'v'])):
    __slots__ = ()
    def __new__(self, u, v):
        return super(P2, self).__new__(self, float(u), float(v))
    def __repr__(self):
        return "P2(%s, %s)" % (self.u, self.v)
    def __add__(self, a):
        return P2(self.u + a.u, self.v + a.v)
    def __sub__(self, a):
        return P2(self.u - a.u, self.v - a.v)
    def __mul__(self, a):
        return P2(self.u*a, self.v*a)
    def __neg__(self):
        return P2(-self.u, -self.v)
    def __rmul__(self, a):
        raise TypeError
    def Lensq(self):
        return self.u*self.u + self.v*self.v
    def Len(self):
        if self.u == 0.0:  return abs(self.v)
        if self.v == 0.0:  return abs(self.u)
        return math.sqrt(self.u*self.u + self.v*self.v)
        
    @staticmethod
    def Dot(a, b):
        return a.u*b.u + a.v*b.v

    @staticmethod
    def ZNorm(v):
        ln = v.Len()
        if ln == 0.0:  
            ln = 1.0
        return P2(v.u/ln, v.v/ln)
            
    @staticmethod
    def APerp(v):
        return P2(-v.v, v.u)
    @staticmethod
    def CPerp(v):
        return P2(v.v, -v.u)



    

# generate the G-code out and splice it back into the output file
def GenerateGcodeMiddle(linarcseq, bXreflected):
    xfac = -1 if bXreflected else 1
    outputlines = [ "G1 X%.3F Y%.3F" % (linarcseq[0][0][0]*xfac, linarcseq[0][0][1])  ]
    for linarc in linarcseq:
        if len(linarc) == 2:
            outputlines.append("G1 X%.3F Y%.3F" % (linarc[1][0]*xfac, linarc[1][1]))
        else:
            assert len(linarc) == 3
            vc = linarc[1] - linarc[0]
            bcircdir = (P2.Dot(P2.CPerp(linarc[2]-linarc[0]), vc) > 0)
            dc = 2 if (bcircdir != bXreflected) else 3
            outputlines.append("G%d X%.3F Y%.3F I%.3F J%.3F" % (dc, linarc[2].u*xfac, linarc[2].v, vc[0]*xfac, vc[1]))
    return '\n'.join(outputlines)
    
    
def ParseFile(filein):
    if re.search("(?i)\.xml$", filein):
        return ParseXMLFile(filein)
    fin = open(filein)
    flist = [(line, re.search(".*?G1 X([\-\.e\d]+) Y([\-\.e\d]+)", line))  for line in fin ]
    fin.close()
    g1iseqs = [ i  for i in range(len(flist)+1)  if (i == 0 or flist[i-1][1] is None) != (i == len(flist) or flist[i][1] is None) ]
    g1seqpairs = [(g1iseqs[j-1], g1iseqs[j])  for j in range(1, len(g1iseqs), 2)]
    assert g1seqpairs, "No sequence of G1 motions found in file"
    i0, i1 = max(g1seqpairs, key=lambda X:X[1]-X[0])
    pts = [ P2(float(m.group(1)), float(m.group(2)))  for l, m in flist[i0:i1] ]
    gcodeheader = "".join(line  for line, m in flist[:i0])
    gcodefooter = "".join(line  for line, m in flist[i1:])
    return gcodeheader, pts, gcodefooter

def ParseXMLFile(filein):
    fs = open(filein).read()
    kfs = re.findall("<Probedata>\s*<Probexvalue>(.*?)</Probexvalue>\s*<Probezvalue>(.*?)</Probezvalue>\s*</Probedata>", fs)
    pts = [ P2(float(l[0]), float(l[1]))  for l in kfs]

    ovals = re.findall("<Probe([A-Z]+)([xz])pos>(.*?)</Probe", fs)
    opts = { }
    for i in range(0, len(ovals), 2):
        assert ovals[i+1][0] == ovals[i][0]
        assert (ovals[i][1], ovals[i+1][1]) == ("x", "z")
        opts[ovals[i][0]] = (float(ovals[i][2]), float(ovals[i+1][2]))
    mss = re.search("<Spindlespeed>(\d+)</Spindlespeed>", fs)
    assert mss, "<Spindlespeed> missing"
    spindlespeed = int(mss.group(1))
    assert "LI" in opts, "<ProbeLIxpos> lead in missing"
    gcodeheader = "%%\nG53\nG0X-10Y-10\nG54\nF500\nG0Y%.3f\nG1X%.3f\nS%d\nM3\nF180=R0\n" % (opts["LI"][1], opts["LI"][0], spindlespeed)
    gcodefooter = "G0Y%.3f\nM5\nG53\nG0Y-10\nG0X-10\nM30\n" % opts["CP"][1]
    return gcodeheader, pts, gcodefooter


def MakeXAscending(pts):
    if pts[0].u < pts[-1].u:
        return pts, False
    return [P2(-p.u, p.v)  for p in pts], True
    

# segmentation work
def slopecurve(pts, i, ir):
    ps = pts[max(i-ir,0):min(i+ir+1,len(pts))]
    Nfac = 1.0/len(ps)
    su, sv = sum(p.u  for p in ps)*Nfac, sum(p.v  for p in ps)*Nfac
    su2, suv = sum(p.u**2  for p in ps)*Nfac, sum(p.u*p.v  for p in ps)*Nfac
    m = (suv - su*sv) / (su2 - su**2)
    c = (sv - m*su)
    miderr = pts[i].v - (m*pts[i].u + c)
    return m, miderr
    
def convolve(seq, kern):
    res = [ ]
    for i in range(len(seq)):
        sv, s = kern[0]*seq[i], kern[0]
        for j in range(1, len(kern)):
            if i+j < len(seq):
                sv += kern[j]*seq[i+j]
                s += kern[j]
            if i-j >= 0:
                sv += kern[j]*seq[i-j]
                s += kern[j]
        res.append(sv/s)
    return res

def DeriveSlopesCurves(pts, slopemeasuresamplewidth):
    kernWide = [math.exp(-x**2*0.04)  for x in range(11)]
    kernNarrow = [math.exp(-x**2*0.2)  for x in range(5)]

    scs = [slopecurve(pts, i, slopemeasuresamplewidth)  for i in range(len(pts))]
    slopes = [ sc[0]  for sc in scs ]
    curvature = [ sc[1]  for sc in scs ]
    csmooth = convolve(curvature, kernWide)
    crough = convolve(curvature, kernNarrow)
    sendactivity(contours=[[(pts[i][0], curvature[i]*200)  for i in range(len(pts))]], materialnumber=3)
    return slopes, csmooth, crough


def FindInflexPoints(csmooth):
    cinflex = [ 0 ]
    for i in range(1, len(csmooth)):
        if (csmooth[i] < 0) != (csmooth[cinflex[-1]] < 0):
            cinflex.append(i)
    cinflex.append(len(csmooth))
    return cinflex


def ShiftInflexToRoughPoints(lcinflex, csmooth, crough):
    cinflex = lcinflex[:]
    for j in range(1, len(cinflex)-1):
        bdown = (csmooth[cinflex[j]] < 0)
        for nv in (cinflex[j] + ik*d  for ik in range(10)  for d in [-1,1]):
            if not cinflex[j-1] < nv < cinflex[j+1]:
                continue
            if ((crough[nv-1] < 0) != bdown) and ((crough[nv] < 0) == bdown):
                cinflex[j] = nv
                break
    return cinflex

def RemoveInflexInConstRegions(lcinflex, slopes, minslopediff):
    cinflex = lcinflex[:]
    def rg(L):
        return max(L)-min(L)
    while len(cinflex) > 2:
        slopediff, j = min((rg([slopes[i]  for i in range(cinflex[j-1], cinflex[j+1])]), j)   for j in range(1, len(cinflex)-1))
        if slopediff > minslopediff:
            break
        del cinflex[j]
    return cinflex, slopediff

class CPIinflex:
    def __init__(self, i, slope, pt, direction):
        self.i = i
        self.slope = slope
        self.pt = pt
        self.direction = direction
        self.bics = None        

def ExtractInflexPointSlopes(cinflex, pts, slopes):
    cpinflex = [ ]
    for j in range(len(cinflex)):
        ci = cinflex[j]
        if ci == len(pts):
            ci -= 1
        if j != 0:
            dr = "curveup" if (slopes[ci] > slopes[cinflex[j-1]]) else "curvedown"
        else:
            dr = "start"
        cpinflex.append(CPIinflex(cinflex[j], slopes[ci], pts[ci], dr))
    return cpinflex

def lintoll(p0, p, p1):
    lam = (p.u - p0.u)/(p1.u - p0.u)
    return abs(p0.v*(1-lam) + p1.v*lam - p.v)

def AssignLinesPInflex(cpinflex, pts, lineartol):
    def tolrg(j):
        p0, p1 = cpinflex[j-1].pt, cpinflex[j].pt
        return max(lintoll(p0, pts[i], p1)  for i in range(cpinflex[j-1].i, cpinflex[j].i))
    while len(cpinflex)>2:
        tl = [(tolrg(j), j)  for j in range(1, len(cpinflex))  if cpinflex[j].direction != "straight"]
        if not tl:
            break
        mtl = min(tl)
        if mtl[0] > lineartol:
            break
        j = mtl[1]
        p0, p1 = cpinflex[j-1].pt, cpinflex[j].pt
        slope = (p1.v - p0.v)/(p1.u - p0.u)
        cpinflex[j-1].slope = slope
        cpinflex[j].slope = slope
        cpinflex[j].direction = "straight"

def RemoveShortStraightsInCurves(lcpinflex, width):
    cpinflex = lcpinflex[:]
    j = 0
    while j < len(cpinflex) - 2:
        j += 1
        if cpinflex[j].direction != "straight":
            continue
        if cpinflex[j].pt.u - cpinflex[j-1].pt.u > width:
            continue
            
        if j == 1:
            if cpinflex[j+1].direction != "straight":
                del cpinflex[j]
                j = 0
        elif j == len(cpinflex) - 1:
            if cpinflex[j-1].direction != "straight":
                cpinflex[j].direction = cpinflex[j-1].direction
                del cpinflex[j-1]
                j = 0
        else:
            if cpinflex[j-1].direction == cpinflex[j+1].direction and cpinflex[j+1].direction != "straight":
                del cpinflex[j-1:j+1]
                j = 0
    return cpinflex

def MergeSameDirectionCurves(lcpinflex):
    cpinflex = lcpinflex[:]
    j = 0
    while j < len(cpinflex) - 2:
        if cpinflex[j].direction != "straight" and cpinflex[j].direction == cpinflex[j+1].direction:
            del cpinflex[j]
        else:
            j += 1
    return cpinflex


def Getstats(pts, linarcseq):
    res = [ "; GCode file processed by %s" % sys.argv[0] ]
    rad0s = [ (linarc[0]-linarc[1]).Len()  for linarc in linarcseq  if len(linarc)==3 ]
    rad2s = [ (linarc[2]-linarc[1]).Len()  for linarc in linarcseq  if len(linarc)==3 ]
    raderr = min(abs(r0-r1)  for r0,r1 in zip(rad0s, rad2s))
    perr = [ ]
    nlines = sum(1  for linarc in linarcseq  if len(linarc)==2)
    narcs = sum(1  for linarc in linarcseq  if len(linarc)==3)
    res.append("; %d lines converted to %d lines and %d arcs" % (len(pts)-1, nlines, narcs))
    for p in pts:
        for linarc in linarcseq:
            p0, p2 = linarc[0], linarc[-1]
            if (p0.u<=p.u<=p2.u  if p0.u<=p2.u  else  p2.u<=p.u<=p0.u):
                break
        lam = (p.u - p0.u)/(p2.u - p0.u)
        assert -1e-5<=lam<=1+1e-5, (p, lam)
        if len(linarc) == 2:
            perr.append(abs(p0.v + lam*(p2.v - p0.v) - p.v))
        else:
            r = (p0 - linarc[1]).Len()
            perr.append(abs((p - linarc[1]).Len() - r))
    res.append("; to tolerance %.3F" % max(perr))
    return "\n".join(res)
        
        
        
    
sendactivity("clearalltriangles")
sendactivity("clearallpoints")
sendactivity("clearallcontours")
sendactivity(contours=[[(-300,0),(900,0)]])
Main()  # function call at end so we can work on code at top of file

