import re

filein = "/home/goatchurch/geom3d/arcfithorizontalprofile/testdata/laquer2.Cnc"
fname = "/home/goatchurch/geom3d/arcfithorizontalprofile/testdata/laquer2.xml"

fin = open(filein)
flist = [(line, re.search(".*?G1 X([\-\.e\d]+) Y([\-\.e\d]+)", line))  for line in fin ]
fin.close()
g1iseqs = [ i  for i in range(len(flist)+1)  if (i == 0 or flist[i-1][1] is None) != (i == len(flist) or flist[i][1] is None) ]
g1seqpairs = [(g1iseqs[j-1], g1iseqs[j])  for j in range(1, len(g1iseqs), 2)]
assert g1seqpairs, "No sequence of G1 motions found in file"
i0, i1 = max(g1seqpairs, key=lambda X:X[1]-X[0])
pts = [ (float(m.group(1)), float(m.group(2)))  for l, m in flist[i0:i1] ]

sendactivity(contours=[pts])

fs = open(fname).read()
kfs = re.findall("<Probedata>\s*<Probexvalue>(.*?)</Probexvalue>\s*<Probezvalue>(.*?)</Probezvalue>\s*</Probedata>", fs)
kpts = [(float(l[0]), float(l[1]))  for l in kfs]
sendactivity(contours=[kpts], materialnumber=1)

assert len(kpts) == len(pts), ("numbers of points should be equal", len(kpts), len(pts))
mcd = max(max(abs(p[0]-q[0]), abs(p[1]-q[1]))  for p, q in zip(pts, kpts))
print("max coord difference", mcd)


# now compare the header data between the files
ovals = re.findall("<Probe([A-Z]+)([xz])pos>(.*?)</Probe", fs)
opts = { }
for i in range(0, len(ovals), 2):
    assert ovals[i+1][0] == ovals[i][0]
    assert (ovals[i][1], ovals[i+1][1]) == ("x", "z")
    opts[ovals[i][0]] = (float(ovals[i][2]), float(ovals[i+1][2]))
sendactivity(points=list(opts.values()))
opts.keys()

cpts = { }
for line, m in flist:
    mc = re.search("\(([A-Za-z \-]+?) X(.*?), Y(.*?)\)", line)
    if mc:
        cpts[mc.group(1)] = (float(mc.group(2)), float(mc.group(3)))
for cv, p in cpts.items():
    scv = "".join(re.findall("[A-Z]", cv))  # just caps
    print(cv, scv, p, opts.get(scv))
    
# Retract Position is unaccounted for and is 58.246mm above Lead Out point
    


